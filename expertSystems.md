# History Is Repeating Itself 

> ...Artificial intelligence (AI) is on the verge of transforming the way we do business. 
[Thinking About Artificial Intellgence](https://hbr.org/1987/07/thinking-about-artificial-intelligence) - HBR July 1987

> Artificial intelligence is reshaping business—though not at the blistering pace many assume.
[Building The AI Powered Organization](https://hbr.org/2019/07/building-the-ai-powered-organization) - HBR July 2019 


> But it is still a long way from delivering on the more extravagant claims that some have made for it. 
[Thinking About Artificial Intellgence](https://hbr.org/1987/07/thinking-about-artificial-intelligence) - HBR July 1987

AI is indeed moving from the research lab into business, industrial, and professional applications. 

 True, AI is now guiding decisions on everything from crop harvests to bank loans, and once pie-in-the-sky prospects such as totally automated customer service are on the horizon.

Yet, although many seemingly “obvious” applications of AI are unlikely ever to happen, there are some very effective and near-term ones that are so subtle they’re apt to be overlooked. Knowing one from the other and recognizing the unique problems of deploying AI in a large organization are the keys to understanding the real potential of this new technology.

[Thinking About Artificial Intellgence](https://hbr.org/1987/07/thinking-about-artificial-intelligence) - HBR July 1987 
