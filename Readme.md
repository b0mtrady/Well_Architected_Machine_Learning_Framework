# The Well-Architected Machine Learning Framework

[Microsoft](https://docs.microsoft.com/en-us/azure/architecture/framework/) and [Amazon](https://aws.amazon.com/architecture/well-architected/?wa-lens-whitepapers.sort-by=item.additionalFields.sortDate&wa-lens-whitepapers.sort-order=desc) have agreed, there is a need for "Well-Architected Frameworks."

Amazon tells us that the Well-Architected framework:
> Helps cloud architects build secure, high-performing, resilient, and efficient infrastructure

Microsoft that...

> "A Well Architected framework are guiding tenants...to improve the quality of a workload."

But what is a workload? What do we mean by "secure", "resilient", "efficent"?

## Well Architected Frameworks...Twins!!!

To understand the totality of the Well-Architected Framework we must understand
where Amazon and Microsfot diverge in their thinking.

Both Well-Architected version are based upon "pillars":

**Amazon Well-Architected 5 Pillars**:
* Operational Excellence
* Security
* Reliability
* Performance Efficiency
* Cost Optimization 

**Microsoft Well-Architected 5 Pillars**:
* Operational Excellence
* Security
* Reliability
* Performance Efficiency
* Cost Optimization

Wait a minute...something seems familiar here. 

<div><figure><img src="https://i0.wp.com/farm2.static.flickr.com/1158/1453262748_63937d8361_o.gif?zoom=2"><figcaption>Amzn + MSFT Well-Architected Strolling Down the Street</figcaption></figure></div>

## AWS Well-Architected Framework

Amazon's Well-Architected Framework was originally introduced in 2012 with eight versions having been released, the most recent in July 2020. The first three versions were internal only release with the 2015 Version 4 released as a white paper to the public. 

> The process for reviewing an architecture is a constructive conversation about architectural decisions, and is not an audit mechanism.

> The Well-Architected Framework emphasizes learning, measuring, and improving. --- (It is a build, measure, learn)

> The AWS Well-Architected Framework documents a set of foundational questions that allow you to understand if a specific architecture aligns well with cloud best practices.

> The framework provides a consistent approach to evaluating systems against the qualities you expect from modern cloud-based systems, and the remediation that would be required to achieve those qualities. 

> Teams often use the [TOGAF](https://pubs.opengroup.org/architecture/togaf9-doc/arch/?ref=wellarchitected-wp) or [Zachman](https://www.zachman.com/about-the-zachman-framework?ref=wellarchitected-wp) framework for Enterprise Architecture. 

> At AWS, we prefer to distribute capabilities into teams rather than having a centralized team with that capability. There are risks when you choose to distribute decision making authority, for example, ensure that teams are meeting internal standards. We mitigate these risks in two ways. First, we have practices that focus on enabling each team to have that capability, and we put in place experts who ensure that teams raise the bar on the standards they need to meet. Second, we implement mechanisms that carry out automated checks to ensure standards are being met. 

> To help new teams gain these capabilities or existing teams to raise their bar, we enable access to a virtual community of principal engineers who can review their designs and help them understand what AWS best practices are. The principal engineering community works to make best practices visible and accessible. One way they do this, for example, is through lunchtime talks that focus on applying best practices to real examples. These talks are recorded and can be used as part of onboarding materials for new team members.

> By following the approach of a principal engineering community with distributed ownership of architecture, we believe that a Well-Architected enterprise architecture can emerge that is driven by customer need... Using this approach, you can identify themes across teams that your organization could address by mechanisms, training, or lunchtime talks where your principal engineers can share their thinking on specific areas with multiple teams.

General Design Principles:
* Stop guessing your capacity needs
* Test systems at production scale
* Automate to make architectural experimentation easier
* Allow for evolutionary architectures
* Drive architectures using data
* Improve through game days

Five Pillars Questions

* OPS 1  How do you determine what your priorities are?
* OPS 2  How do you structure your organization to support your business outcomes?
* OPS 3  How does your organizational culture support your business outcomes?
* OPS 4  How do you design your workload so that you can understand its state?
* OPS 5  How do you reduce defects, ease remediation, and improve flow into production?
* OPS 6  How do you mitigate deployment risks?
* OPS 7  How do you know that you are ready to support a workload?
* OPS 8  How do you understand the health of your workload?
* OPS 9  How do you understand the health of your operations?
* OPS 10  How do you manage workload and operations events?
* OPS 11  How do you evolve operations?
<br>

* SEC 1  How do you securely operate your workload?
* SEC 2  How do you manage authentication for people and machines?
* SEC 3  How do you manage permissions for people and machines?
* SEC 4  How do you detect and investigate security events?
* SEC 5  How do you protect your network resources?
* SEC 6  How do you protect your compute resources?
* SEC 7  How do you classify your data?
* SEC 8  How do you protect your data at rest?
* SEC 9  How do you protect your data in transit?
* SEC 10  How do you anticipate, respond to, and recover from incidents?
<br> 

* REL 1  How do you manage service quotas and constraints?
* REL 2  How do you plan your network topology?
* REL 3  How do you design your workload service architecture?
* REL 4  How do you design interactions in a distributed system to prevent failures?
* REL 5  How do you design interactions in a distributed system to mitigate or withstand failures?
* REL 6  How do you monitor workload resources?
* REL 7  How do you design your workload to adapt to changes in demand?
* REL 9  How do you back up data?
* REL 10  How do you use fault isolation to protect your workload?
* REL 11  How do you design your workload to withstand component failures?
* REL 12  How do you test reliability?
* REL 13  How do you plan for disaster recovery (DR)?
<br>

* PERF 1  How do you select the best performing architecture?
* PERF 2  How do you select your compute solution?
* PERF 3  How do you select your storage solution?
* PERF 4  How do you select your database solution?
* PERF 5  How do you configure your networking solution?
* PERF 6  How do you evolve your workload to take advantage of new releases?
* PERF 7  How do you monitor your resources to ensure they are performing?
* PERF 8  How do you use tradeoffs to improve performance?
<br>

* COST 1  How do you implement cloud financial management?
* COST 2  How do you govern usage?
* COST 3  How do you monitor usage and cost?
* COST 4  How do you decommission resources?
* COST 5  How do you evaluate cost when you select services?
* COST 6  How do you meet cost targets when you select resource type, size and number?
* COST 7  How do you use pricing models to reduce cost?
* COST 8  How do you plan for data transfer charges?
* COST 9  How do you manage demand, and supply resources?
* COST 10  How do you evaluate new services?



## Operational Excellence

[White Paper](https://d1.awsstatic.com/whitepapers/architecture/AWS-Operational-Excellence-Pillar.pdf)

### Design Principles:
* **Perform operations as code**
    * define your entire workload (applications, infrastructure, etc.) as code and update
it with code 
    * declaritive not imperative 
* **Make frequent, small, reversible changes**
* **Refine operations procedures frequently**
    * Continuous Improvement in procedures
    * Involve the team in validating procedure Optimization
    * Gamification
* **Anticipate failure**
    *  “Pre-mortem” exercises to identify potential sources of failure so that they can be removed or mitigated.
    * Test failure scenario procedures by simulated events
    * Test failure response by simulated events 
    * Gamification
* **Learn from all operational failures**
    * Share lessons learned across the organization 

**Definition**

> Your organization’s leadership defines business objectives. Your organization must
understand requirements and priorities and use these to organize and conduct work to
support the achievement of business outcomes.

* Organization
* Prepare
* Operate
* Evolve

**Organization**

> Your teams need to have a shared understanding of your entire workload, their role in it,
and shared business goals to set the priorities that will enable business success. Welldefined priorities will maximize the benefits of your efforts. Review your priorities
regularly so that they can be updated as needs change.

* Organization Priorities
    * Evaluate **external customer needs**
    * Evaluate **internal customer needs**
    * Evaluate **governance requirements**: Ensure that you are aware of guidelines or obligations defined by your organization that may mandate or emphasize specific focus. 
    * Evaluate **external compliance requirements**: Evaluate external factors, such as regulatory compliance requirements and industry standards. Validate that you
have mechanisms to identify changes to compliance requirements.
    * Evaluate **threat landscape**: Evaluate threats to the business (for example, competition,
business risk and liabilities, operational risks, and information security threats)
    * Evaluate **tradeoffs**: Evaluate the impact of tradeoffs between competing interests or
alternative approaches, to help make informed decisions when determining where to
focus operations efforts or choosing a course of action. 
    * Manage **benefits and risks**: Manage benefits and risks to make informed decisions
when determining where to focus efforts. Review your priorities regularly and
update your priorities as needs change.
* Operating Model
* Organizational Culture 

## Security

## Reliability

## Performance Efficiency

## Cost Optimization
